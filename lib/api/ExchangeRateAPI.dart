import 'package:dio/dio.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';
import '../data/ExchangeRate.dart';

class ExchangeRateAPI {
  final dio = Dio();

  Future<ExchangeRate?> getExchangeRate(String baseCurrency) async {
    var url = "https://api.exchangerate-api.com/v4/latest/$baseCurrency";
    dio.interceptors.add(
      PrettyDioLogger(
        requestHeader: true,
        requestBody: true,
        responseBody: true,
        responseHeader: true,
        error: true,
        compact: true,
        maxWidth: 90,
      ),
    );
    var response = await dio.get(url);
    var dataExchangeRate = exchangeRateFromJson(response.data);
    return dataExchangeRate;
  }
}
