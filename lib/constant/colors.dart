import 'package:flutter/painting.dart';

class AppColors {
  static const Color colorDivider = Color(0xFFEEEEEE);
  static const Color colorPrimary = Color(0xFF13BB9A);
  static const Color colorBlack = Color(0xFF000000);
  static const Color colorWhite = Color(0xFFFFFFFF);
  static const Color colorLightGray = Color(0xFFB5C1D7);
  static const Color colorGray = Color(0xFF828C9E);
  static const Color colorDarkGray = Color(0xFF35383E);
  static const Color colorRedBright = Color(0xFFFF375F);
  static const Color colorPurple = Color(0xFF8E24AA);
  static const Color colorBlue = Color(0xFF1E88E5);

  static const LinearGradient colorPrimaryGradient = LinearGradient(colors: [Color(0xFF13BB9A), Color(0xFF098DD6)]);
}
