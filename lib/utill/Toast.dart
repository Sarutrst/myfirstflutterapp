import 'package:fluttertoast/fluttertoast.dart';

showToast(String message) {
  Fluttertoast.cancel();
  Fluttertoast.showToast(
    msg: message,
    toastLength: Toast.LENGTH_SHORT,
    gravity: ToastGravity.BOTTOM, // location
    timeInSecForIosWeb: 2, // duration
  );
}
