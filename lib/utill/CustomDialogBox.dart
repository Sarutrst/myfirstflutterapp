import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:myfirstflutter/widget/CustomElevatedButton.dart';

class CustomDialogBox extends StatelessWidget {
  final String? title, descriptions, positiveText, negativeText;
  final String? image;
  final Color? positiveButtonColor, negativeButtonColor;
  final GestureTapCallback? onSuccess, onDismiss;

  const CustomDialogBox({
    Key? key,
    this.title,
    this.descriptions,
    this.positiveText,
    this.negativeText,
    this.positiveButtonColor,
    this.negativeButtonColor,
    this.image,
    this.onDismiss,
    required this.onSuccess,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(16),
      ),
      elevation: 0,
      backgroundColor: Colors.transparent,
      child: contentBox(context),
    );
  }

  contentBox(context) {
    return Stack(
      children: <Widget>[
        Container(
          padding: const EdgeInsets.all(8),
          decoration: BoxDecoration(
            shape: BoxShape.rectangle,
            color: Colors.white,
            borderRadius: BorderRadius.circular(8),
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(
                title ?? "",
                style: const TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
              ),
              image != null
                  ? Column(
                      children: [
                        const SizedBox(
                          height: 20,
                        ),
                        Container(
                          constraints: const BoxConstraints(
                            maxHeight: 300,
                          ),
                          child: CachedNetworkImage(
                            imageUrl: image ?? "",
                            placeholder: (context, url) => const CircularProgressIndicator(),
                            errorWidget: (context, url, error) => const Icon(Icons.image_not_supported),
                          ),
                        ),
                      ],
                    )
                  : const SizedBox.shrink(),
              const SizedBox(
                height: 20,
              ),
              Text(
                descriptions ?? "",
                style: const TextStyle(fontSize: 14),
                textAlign: TextAlign.center,
              ),
              const SizedBox(
                height: 20,
              ),
              Row(
                children: [
                  Expanded(
                    child: CustomElevatedButton(
                      buttonColor: negativeButtonColor ?? Colors.grey,
                      buttonText: negativeText,
                      isSingleLine: true,
                      onPress: () {
                        Navigator.of(context).pop();

                        if (onDismiss != null) {
                          onDismiss!();
                        }
                      },
                    ),
                  ),
                  const SizedBox(
                    width: 8,
                  ),
                  Expanded(
                    child: CustomElevatedButton(
                      buttonColor: positiveButtonColor ?? Colors.blueAccent,
                      buttonText: positiveText,
                      isSingleLine: true,
                      onPress: () {
                        if (onSuccess != null) {
                          onSuccess!();
                        }

                        Navigator.of(context).pop();
                      },
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }
}
