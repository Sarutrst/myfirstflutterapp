import 'package:flutter/material.dart';

showSnackBar(BuildContext context, String message) {
  ScaffoldMessenger.of(context).hideCurrentSnackBar();
  ScaffoldMessenger.of(context).showSnackBar(
    SnackBar(
      content: Text(message),
    ),
  );
}

showSnackWithAction(BuildContext context, String message, String label, String? actionMessage, Function() updateValue) {
  ScaffoldMessenger.of(context).hideCurrentSnackBar();
  ScaffoldMessenger.of(context).showSnackBar(SnackBar(
    content: Text(message),
    action: SnackBarAction(
      label: label,
      onPressed: () {
        updateValue();
        ScaffoldMessenger.of(context).hideCurrentSnackBar();
        actionMessage != null
            ? ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(
                  content: Text(
                    actionMessage,
                  ),
                ),
              )
            : null;
      },
    ),
  ));
}
