import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:myfirstflutter/utill/Toast.dart';

import '../constant/colors.dart';
import '../constant/routes.dart';

class ThirdPage extends StatefulWidget {
  const ThirdPage({super.key});

  @override
  State<ThirdPage> createState() => _ThirdPageState();
}

class _ThirdPageState extends State<ThirdPage> with RestorationMixin {
  final RestorableDateTime _fromDate = RestorableDateTime(DateTime.now());
  final RestorableTimeOfDay _fromTime = RestorableTimeOfDay(
    TimeOfDay.fromDateTime(DateTime.now()),
  );
  final RestorableDateTime _startDate = RestorableDateTime(DateTime.now());
  final RestorableDateTime _endDate = RestorableDateTime(DateTime.now());

  late RestorableRouteFuture<DateTime> _restorableDatePickerRouteFuture;
  late RestorableRouteFuture<DateTimeRange> _restorableDateRangePickerRouteFuture;
  late RestorableRouteFuture<TimeOfDay> _restorableTimePickerRouteFuture;

  var type = 0;
  var unixTime = "";

  void _selectDate(DateTime selectedDate) {
    if (selectedDate != _fromDate.value) {
      setState(() {
        _fromDate.value = selectedDate;
      });
    }
  }

  void _selectDateRange(DateTimeRange newSelectedDate) {
    setState(() {
      _startDate.value = newSelectedDate.start;
      _endDate.value = newSelectedDate.end;
    });
  }

  void _selectTime(TimeOfDay selectedTime) {
    if (selectedTime != _fromTime.value) {
      setState(() {
        _fromTime.value = selectedTime;
      });
    }
  }

  static Route<DateTime> _datePickerRoute(
    BuildContext context,
    Object? arguments,
  ) {
    return DialogRoute<DateTime>(
      context: context,
      builder: (context) {
        return DatePickerDialog(
          restorationId: 'date_picker_dialog',
          initialDate: DateTime.fromMillisecondsSinceEpoch(arguments as int),
          firstDate: DateTime.now().subtract(const Duration(days: 0)),
          lastDate: DateTime.now().add(const Duration(days: 7)),
        );
      },
    );
  }

  static Route<TimeOfDay> _timePickerRoute(
    BuildContext context,
    Object? arguments,
  ) {
    final args = arguments as List<Object>;
    final initialTime = TimeOfDay(
      hour: args[0] as int,
      minute: args[1] as int,
    );

    return DialogRoute<TimeOfDay>(
      context: context,
      builder: (context) {
        return MediaQuery(
          data: MediaQuery.of(context).copyWith(alwaysUse24HourFormat: true),
          child: TimePickerDialog(
            restorationId: 'time_picker_dialog',
            initialTime: initialTime,
          ),
        );
      },
    );
  }

  static Route<DateTimeRange> _dateRangePickerRoute(
    BuildContext context,
    Object? arguments,
  ) {
    return DialogRoute<DateTimeRange>(
      context: context,
      builder: (context) {
        return DateRangePickerDialog(
          restorationId: 'date_rage_picker_dialog',
          firstDate: DateTime.now().subtract(const Duration(days: 0)),
          lastDate: DateTime(DateTime.now().year + 5),
        );
      },
    );
  }

  @override
  void initState() {
    super.initState();
    _restorableDatePickerRouteFuture = RestorableRouteFuture<DateTime>(
      onComplete: _selectDate,
      onPresent: (navigator, arguments) {
        return navigator.restorablePush(
          _datePickerRoute,
          arguments: _fromDate.value.millisecondsSinceEpoch,
        );
      },
    );
    _restorableDateRangePickerRouteFuture = RestorableRouteFuture<DateTimeRange>(
      onComplete: _selectDateRange,
      onPresent: (navigator, arguments) => navigator.restorablePush(_dateRangePickerRoute),
    );

    _restorableTimePickerRouteFuture = RestorableRouteFuture<TimeOfDay>(
      onComplete: _selectTime,
      onPresent: (navigator, arguments) => navigator.restorablePush(
        _timePickerRoute,
        arguments: [_fromTime.value.hour, _fromTime.value.minute],
      ),
    );

    print(DateTime.now().toUtc().millisecondsSinceEpoch);
    var unixTimeStamp = 1689059279921;
    var dt = DateTime.fromMillisecondsSinceEpoch(unixTimeStamp);
    // 12 Hour format:
    var d12 = DateFormat('dd/MM/yyyy, hh:mm:ss a').format(dt);
    // 24 Hour format:
    var d24 = DateFormat('dd/MM/yyyy, HH:mm:ss').format(dt);
    print("12 hrs : $d12");
    print("24 hrs : $d24");
  }

  @override
  String get restorationId => 'picker_demo';

  @override
  void restoreState(RestorationBucket? oldBucket, bool initialRestore) {
    registerForRestoration(_fromDate, 'from_date');
    registerForRestoration(_fromTime, 'from_time');
    registerForRestoration(_startDate, 'start_date');
    registerForRestoration(_endDate, 'end_date');
    registerForRestoration(
      _restorableDatePickerRouteFuture,
      'date_picker_route',
    );
    registerForRestoration(
      _restorableDateRangePickerRouteFuture,
      'date_range_picker_route',
    );
    registerForRestoration(
      _restorableTimePickerRouteFuture,
      'time_picker_route',
    );
  }

  String get _labelText {
    switch (type) {
      case 0:
        //date
        return DateFormat.yMMMd().format(_fromDate.value);
      case 1:
        //time
        return MaterialLocalizations.of(context).formatTimeOfDay(_fromTime.value, alwaysUse24HourFormat: true);
      case 2:
        //range
        return '${DateFormat.yMMMd().format(_startDate.value)} - ${DateFormat.yMMMd().format(_endDate.value)}';
    }
    return "";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
        leading: GestureDetector(
          child: const Icon(
            Icons.arrow_back,
            color: AppColors.colorBlack,
          ),
          onTap: () {
            Navigator.pop(context);
          },
        ),
        title: const Text("Picker"),
      ),
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            GestureDetector(
              onLongPress: () async {
                await Clipboard.setData(const ClipboardData(text: "copy ja"));
                showToast("copy success.");
              },
              child: Text("Unix DateTime Now : ${DateTime.now().toUtc().millisecondsSinceEpoch.toString()}"),
            ),
            const SizedBox(height: 16),
            Text(
              "Value Selected : $_labelText",
              style: const TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
            ),
            const SizedBox(height: 16),
            ElevatedButton(
              onPressed: () {
                switch (type) {
                  case 0:
                    //date
                    _restorableDatePickerRouteFuture.present();
                    break;
                  case 1:
                    //time
                    _restorableTimePickerRouteFuture.present();
                    break;
                  case 2:
                    //range
                    _restorableDateRangePickerRouteFuture.present();
                    break;
                }
              },
              child: const Text(
                "Show Picker",
              ),
            ),
            ElevatedButton(
              onPressed: () {
                Navigator.popUntil(context, ModalRoute.withName(AppRoutes.firstPage));
              },
              child: const Text(
                'Back to First Page',
                style: TextStyle(color: AppColors.colorWhite),
              ),
            )
          ],
        ),
      ),
    );
  }
}
