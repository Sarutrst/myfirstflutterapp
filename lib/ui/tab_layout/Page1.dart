import 'package:flutter/material.dart';
import 'package:myfirstflutter/constant/colors.dart';
import 'package:myfirstflutter/ui/ThirdPage.dart';
import 'package:myfirstflutter/utill/CustomDialogBox.dart';
import 'package:myfirstflutter/utill/Toast.dart';
import 'package:myfirstflutter/widget/CustomElevatedButton.dart';
import '../../constant/routes.dart';
import '../../utill/SnackBar.dart';

class Page1 extends StatefulWidget {
  const Page1({super.key});

  @override
  State<Page1> createState() => _Page1State();
}

class _Page1State extends State<Page1> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(10),
      child: Column(
        children: [
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  CustomElevatedButton(
                    buttonText: "Show Dialog",
                    buttonColor: AppColors.colorPurple,
                    buttonRadius: 32,
                    onPress: () {
                      _showDialog();
                    },
                  ),
                  CustomElevatedButton(
                    buttonText: "Show Custom Dialog",
                    buttonColor: AppColors.colorPurple,
                    buttonRadius: 32,
                    onPress: () {
                      _showCustomDialog(context);
                    },
                  ),
                ],
              ),
            ),
          ),
          const Divider(
            color: AppColors.colorLightGray,
            height: 20,
            thickness: 1,
          ),
          Expanded(
            flex: 2,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  CustomElevatedButton(
                    buttonText: "Custom Button",
                    buttonColor: AppColors.colorRedBright,
                    onPress: () {},
                  ),
                  ElevatedButton(
                    onPressed: () {
                      showSnackWithAction(context, "Page1 SnackBar With Action", "ThirdPage", "Go to ThirdPage", () {
                        Navigator.pushNamed(context, AppRoutes.thirdPage);
                      });
                    },
                    child: const Text("Show Snack Bar With Action"),
                  ),
                  ElevatedButton(
                    onPressed: () {
                      showSnackWithAction(context, "Page1 SnackBar With Action", "ThirdPage", null, () {
                        showSnackBar(context, "Joking");
                      });
                    },
                    child: const Text("Show Snack Bar With Action"),
                  ),
                  ElevatedButton(
                    onPressed: () {
                      showSnackBar(context, "Page1 SnackBar");
                    },
                    child: const Text("Show Snack Bar"),
                  ),
                  ElevatedButton(
                    onPressed: () {
                      showToast("Show Toast Message");
                    },
                    child: const Text("Show Toast"),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  _showCustomDialog(BuildContext context) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return CustomDialogBox(
            title: "Custom Dialog Demo",
            descriptions: "Hi all this is a custom dialog in flutter and  you will be use in your flutter applications",
            positiveText: "Confirm",
            negativeText: "Cancel",
            positiveButtonColor: AppColors.colorPrimary,
            negativeButtonColor: AppColors.colorLightGray,
            onSuccess: () {
              showToast("success");
            },
          );
        });
  }

  _showDialog() {
    showDialog<String>(
      context: context,
      builder: (BuildContext context) => AlertDialog(
        title: const Text('AlertDialog Title'),
        content: const Text('AlertDialog description'),
        actions: <Widget>[
          TextButton(
            onPressed: () => Navigator.pop(context, 'Cancel'),
            child: const Text(
              'Cancel',
              style: TextStyle(color: AppColors.colorRedBright),
            ),
          ),
          TextButton(
            onPressed: () => Navigator.pop(context, 'OK'),
            child: const Text(
              'OK',
              style: TextStyle(color: AppColors.colorBlack),
            ),
          ),
        ],
      ),
    );
  }
}
