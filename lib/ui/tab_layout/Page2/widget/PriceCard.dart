import 'package:flutter/material.dart';

class PriceCard extends StatelessWidget {
  final String? fullPrice;
  final String? discountPrice;
  final String? expiredDate;
  final bool? isShowDate;

  const PriceCard(this.fullPrice, this.discountPrice, this.expiredDate, this.isShowDate, {super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(bottom: 4, top: 8, left: 8, right: 8),
      color: Colors.black.withOpacity(0.6),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.end,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Wrap(
            direction: Axis.vertical,
            children: [
              discountPrice != null
                  ? Text(
                      discountPrice ?? "0",
                      style: const TextStyle(
                        color: Colors.white,
                        fontSize: 12,
                        decoration: TextDecoration.lineThrough,
                        fontWeight: FontWeight.bold,
                      ),
                      overflow: TextOverflow.ellipsis,
                      textAlign: TextAlign.start,
                    )
                  : const SizedBox.shrink(),
              Text(
                fullPrice ?? "0",
                style: const TextStyle(color: Colors.white, fontSize: 14, fontWeight: FontWeight.bold),
                overflow: TextOverflow.ellipsis,
                textAlign: TextAlign.start,
              ),
            ],
          ),
          isShowDate ?? false
              ? Flexible(
                  child: Text(
                    expiredDate ?? "",
                    style: const TextStyle(color: Colors.white, fontSize: 10),
                    overflow: TextOverflow.ellipsis,
                    maxLines: 1,
                    textAlign: TextAlign.end,
                  ),
                )
              : const SizedBox.shrink()
        ],
      ),
    );
  }
}
