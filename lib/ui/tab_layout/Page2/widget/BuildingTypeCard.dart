import 'package:flutter/material.dart';

class BuildingTypeCard extends StatelessWidget {
  final String buildingTypeText;
  final Color buildingTypeColor;

  const BuildingTypeCard(this.buildingTypeText, this.buildingTypeColor, {super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: buildingTypeColor,
        borderRadius: BorderRadius.circular(16),
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 2),
        child: Text(
          buildingTypeText,
          style: const TextStyle(fontSize: 12, color: Colors.white),
        ),
      ),
    );
  }
}
