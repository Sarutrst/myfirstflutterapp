import 'package:flutter/material.dart';

class PostTypeCard extends StatelessWidget {
  final String postTypeText;
  final Color postTypeColor;

  const PostTypeCard(this.postTypeText, this.postTypeColor, {super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(16),
        border: Border.all(color: postTypeColor),
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 2),
        child: Text(
          postTypeText,
          style: TextStyle(fontSize: 12, color: postTypeColor),
        ),
      ),
    );
  }
}
