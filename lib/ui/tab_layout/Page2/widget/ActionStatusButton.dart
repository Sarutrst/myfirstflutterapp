import 'package:flutter/material.dart';
import 'package:myfirstflutter/constant/colors.dart';
import 'package:myfirstflutter/widget/CustomElevatedButton.dart';

class ActionStatusButton extends StatelessWidget {
  final int statusType;

  const ActionStatusButton(this.statusType, {super.key});

  @override
  Widget build(BuildContext context) {
    switch (statusType) {
      case 0:
      case 1:
        return Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            children: [
              Expanded(
                child: CustomElevatedButton(
                  buttonColor: AppColors.colorWhite,
                  buttonBorderColorOpacity: 0.2,
                  buttonTextStyle: const TextStyle(color: AppColors.colorPrimary, fontSize: 10),
                  buttonBorderColor: AppColors.colorPrimary,
                  buttonText: "แก้ไข",
                  onPress: () {},
                ),
              ),
              const SizedBox(
                width: 8,
              ),
              Expanded(
                child: CustomElevatedButton(
                  buttonColor: AppColors.colorWhite,
                  buttonBorderColorOpacity: 0.2,
                  buttonTextStyle: const TextStyle(color: AppColors.colorPrimary, fontSize: 10),
                  buttonBorderColor: AppColors.colorPrimary,
                  buttonText: "เพิ่มอายุ",
                  onPress: () {},
                ),
              ),
              const SizedBox(
                width: 8,
              ),
              Expanded(
                child: CustomElevatedButton(
                  buttonColor: AppColors.colorPrimary,
                  buttonTextStyle: const TextStyle(color: AppColors.colorWhite, fontSize: 10),
                  buttonText: "ดันประกาศ",
                  onPress: () {},
                ),
              ),
            ],
          ),
        );
      case 2:
        return Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            children: [
              Expanded(
                child: CustomElevatedButton(
                  buttonColor: AppColors.colorPrimary,
                  buttonTextStyle: const TextStyle(color: AppColors.colorWhite, fontSize: 10),
                  buttonText: "แก้ไข",
                  onPress: () {},
                ),
              ),
            ],
          ),
        );
      case 3:
        return Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            children: [
              Expanded(
                child: CustomElevatedButton(
                  buttonColor: AppColors.colorPrimary,
                  buttonTextStyle: const TextStyle(color: AppColors.colorWhite, fontSize: 10),
                  buttonText: "นำไปใช้ใหม่",
                  onPress: () {},
                ),
              ),
            ],
          ),
        );
      default:
        return const Padding(
          padding: EdgeInsets.all(8.0),
          child: SizedBox.shrink(),
        );
    }
  }
}
