import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:myfirstflutter/constant/colors.dart';
import 'package:myfirstflutter/ui/tab_layout/Page2/widget/ActionStatusButton.dart';
import 'package:myfirstflutter/ui/tab_layout/Page2/widget/PriceCard.dart';
import 'package:myfirstflutter/utill/Toast.dart';
import 'package:myfirstflutter/widget/CustomElevatedButton.dart';
import 'package:social_share/social_share.dart';
import 'widget/BuildingTypeCard.dart';
import 'widget/PostTypeCard.dart';

class Page2 extends StatefulWidget {
  const Page2({super.key});

  @override
  State<Page2> createState() => _Page2State();
}

class _Page2State extends State<Page2> with SingleTickerProviderStateMixin {
  final RxBool _checkVisibleToTop = false.obs;
  final RxBool _checkVisibleMulti = true.obs;
  final RxBool _isLoadMore = false.obs;

  late ScrollController _controller;
  late List<int> listData;
  List<int> listDataMore = [1, 2];
  double position = 0.0;
  double sensitivityFactor = 20.0;

  @override
  void initState() {
    super.initState();
    _controller = ScrollController();
    listData = [1, 2, 3];
  }

  @override
  void dispose() {
    _checkVisibleToTop.close();
    _checkVisibleMulti.close();
    _isLoadMore.close();
    super.dispose();
  }

  Future loadMoreData() async {
    List<int>? newData = await getNewData();
    listData.addAll(newData!);
    _isLoadMore.value = false;
  }

  Future<List<int>?> getNewData() async {
    List<int> data = [];
    await Future.delayed(const Duration(seconds: 2), () {
      data = [...listDataMore];
    });
    return data;
  }

  Future<void> _refresh() async {
    setState(() {
      listData.clear();
      listData.addAll([1, 2, 3]);
    });
  }

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: _refresh,
      child: Scaffold(
        floatingActionButton: Obx(
          () => Visibility(
            visible: _checkVisibleToTop.isTrue,
            child: SizedBox(
              width: 40,
              height: 40,
              child: FloatingActionButton(
                backgroundColor: Colors.transparent,
                elevation: 0,
                onPressed: () => _scrollToTop(),
                child: Image.asset(
                  "assets/images/ic_arrow up.png",
                  width: 40,
                  height: 40,
                ),
              ),
            ),
          ),
        ),
        body: Obx(
          () {
            return NotificationListener<ScrollNotification>(
              onNotification: (ScrollNotification scrollInfo) {
                if (scrollInfo.metrics.pixels > 800) {
                  _checkVisibleToTop.value = true;
                } else {
                  _checkVisibleToTop.value = false;
                }

                if (scrollInfo.metrics.pixels - position >= sensitivityFactor) {
                  position = scrollInfo.metrics.pixels;
                  _checkVisibleMulti.value = false;
                }
                if (position - scrollInfo.metrics.pixels >= 400 ||
                    scrollInfo.metrics.pixels == scrollInfo.metrics.minScrollExtent) {
                  position = scrollInfo.metrics.pixels;
                  _checkVisibleMulti.value = true;
                }

                if ((scrollInfo.metrics.maxScrollExtent - scrollInfo.metrics.pixels) < 400) {
                  if (_isLoadMore.isFalse) {
                    loadMoreData();
                    _isLoadMore.value = true;
                  }
                }

                return false;
              },
              child: SingleChildScrollView(
                controller: _controller,
                child: Column(
                  children: [
                    ListView.builder(
                      shrinkWrap: true,
                      physics: const NeverScrollableScrollPhysics(),
                      itemCount: listData.length,
                      itemBuilder: (BuildContext context, int index) {
                        return Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: myStockItem(),
                        );
                      },
                    ),
                    Visibility(
                      visible: _isLoadMore.isTrue,
                      child: Container(
                        padding: const EdgeInsets.all(8),
                        color: Colors.transparent,
                        child: const Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            CircularProgressIndicator(),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
            );
          },
        ),
        bottomNavigationBar: Obx(
          () => Visibility(
            visible: _checkVisibleMulti.isTrue,
            maintainAnimation: true,
            maintainState: true,
            child: AnimatedSize(
              curve: Curves.fastOutSlowIn,
              duration: const Duration(milliseconds: 200),
              child: Container(
                height: _checkVisibleMulti.isTrue ? null : 0.0,
                color: AppColors.colorWhite,
                padding: const EdgeInsets.all(8.0),
                width: MediaQuery.of(context).size.width,
                child: CustomElevatedButton(
                  buttonColor: AppColors.colorPrimary,
                  buttonTextStyle: const TextStyle(color: AppColors.colorWhite, fontSize: 12),
                  buttonText: "ดันประกาศ/เพิ่มอายุหลายรายการ",
                  gradient: AppColors.colorPrimaryGradient,
                  onPress: () {
                    _showBottomSheet();
                  },
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  _showBottomSheet() {
    showModalBottomSheet(
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(8),
          topRight: Radius.circular(8),
        ),
      ),
      context: context,
      builder: (context) {
        return Wrap(
          children: [
            const Padding(
              padding: EdgeInsets.only(
                top: 24,
                left: 8,
                right: 8,
              ),
              child: Text(
                "เลือก",
                style: TextStyle(fontSize: 14, color: AppColors.colorBlack, fontWeight: FontWeight.bold),
              ),
            ),
            ListTile(
              leading: Image.asset(
                "assets/images/ic_multiboots.png",
                width: 24,
                height: 24,
              ),
              title: const Text(
                'ดันประกาศหลายรายการ',
                style: TextStyle(
                  fontSize: 12,
                  color: AppColors.colorDarkGray,
                ),
              ),
              onTap: () {
                showToast("ดันประกาศหลายรายการ");
                Navigator.pop(context);
              },
            ),
            const Divider(
              height: 20,
              thickness: 2,
            ),
            ListTile(
              leading: Image.asset(
                "assets/images/ic_expent_time.png",
                width: 24,
                height: 24,
              ),
              title: const Text(
                'เพิ่มอายุประกาศหลายรายการ',
                style: TextStyle(
                  fontSize: 12,
                  color: AppColors.colorDarkGray,
                ),
              ),
              onTap: () {
                showToast("เพิ่มอายุประกาศหลายรายการ");
                Navigator.pop(context);
              },
            ),
          ],
        );
      },
    );
  }

  _scrollToTop() {
    _controller.animateTo(
      0,
      curve: Curves.linear,
      duration: const Duration(milliseconds: 500),
    );
    _checkVisibleMulti.value == true;
  }

  Widget myStockItem() {
    return Container(
      decoration: BoxDecoration(
        color: AppColors.colorWhite,
        borderRadius: BorderRadius.circular(16),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 5,
            blurRadius: 8,
          ),
        ],
      ),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        children: [
          AspectRatio(
            aspectRatio: 21 / 9,
            child: Stack(
              fit: StackFit.expand,
              children: [
                ClipRRect(
                  borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(8.0),
                    topRight: Radius.circular(8.0),
                  ),
                  child: Image.asset("assets/images/living_room.png", fit: BoxFit.cover),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          const Row(
                            children: [
                              BuildingTypeCard("บ้าน", Colors.green),
                              SizedBox(
                                width: 4,
                              ),
                              PostTypeCard("ขาย", Colors.green)
                            ],
                          ),
                          Row(
                            children: [
                              Visibility(
                                visible: true,
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: AppColors.colorWhite,
                                    borderRadius: BorderRadius.circular(16),
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.all(2),
                                    child: Image.asset(
                                      "assets/images/ic_pin.png",
                                      width: 24,
                                      height: 24,
                                    ),
                                  ),
                                ),
                              ),
                              const SizedBox(
                                width: 4,
                              ),
                              Container(
                                decoration: BoxDecoration(
                                  color: AppColors.colorWhite,
                                  borderRadius: BorderRadius.circular(16),
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 4),
                                  child: Row(
                                    children: [
                                      Image.asset(
                                        "assets/images/icon_firstpage.png",
                                        width: 20,
                                        height: 20,
                                      ),
                                      const Text(
                                        "100%",
                                        style: TextStyle(fontSize: 10, fontWeight: FontWeight.bold),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    const PriceCard("7,000,000", "10,000,000", "อายุประกาศที่เหลือ 9999 วัน", true)
                  ],
                ),
              ],
            ),
          ),
          _contentView(),
          const ActionStatusButton(1)
        ],
      ),
    );
  }

  Widget _contentView() {
    bool isAgent = false;
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            children: [
              Expanded(
                flex: 9,
                child: Row(
                  children: [
                    Image.asset(
                      "assets/images/icon_autoboost_time.png",
                      width: 20,
                      height: 20,
                    ),
                    const SizedBox(
                      width: 4,
                    ),
                    const Text(
                      "ดันประกาศอัตโนมัติ",
                      style: TextStyle(
                        fontSize: 10,
                      ),
                    ),
                    const SizedBox(
                      width: 4,
                    ),
                    const Text(
                      "ไม่ได้เปิดใช้งาน",
                      style: TextStyle(
                          fontSize: 10, color: AppColors.colorRedBright, decoration: TextDecoration.underline),
                    ),
                  ],
                ),
              ),
              Expanded(
                flex: 1,
                child: GestureDetector(
                  onTap: () {
                    SocialShare.shareOptions("Hello world");
                  },
                  child: Image.asset(
                    "assets/images/icon_menu_share.png",
                    color: AppColors.colorBlack,
                    width: 24,
                    height: 24,
                  ),
                ),
              )
            ],
          ),
        ),
        const Padding(
          padding: EdgeInsets.all(8.0),
          child: Row(
            children: [
              Expanded(
                child: Text(
                  "ให้เช่าคอนโดหรูทำเลพระราม 9 แค่ 7,000 บาท เท่านั้นหาราคาแบบนี้ ในพระราม 9",
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold),
                ),
              )
            ],
          ),
        ),
        Container(
          padding: const EdgeInsets.all(8),
          color: AppColors.colorBlue.withOpacity(0.1),
          child: Row(
            children: [
              Expanded(
                child: Row(
                  children: [
                    Image.asset(
                      "assets/images/ic_map_pin_new.png",
                      width: 20,
                      height: 20,
                    ),
                    const SizedBox(
                      width: 4,
                    ),
                    const Flexible(
                      child: Text(
                        "ไลฟ์ อโศก-พระราม9",
                        style: TextStyle(
                          fontSize: 10,
                          color: AppColors.colorGray,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Wrap(
                children: [
                  Image.asset(
                    "assets/images/ic_map_pin_new.png",
                    width: 20,
                    height: 20,
                  ),
                  const Text(
                    "163,087",
                    style: TextStyle(
                      fontSize: 10,
                      color: AppColors.colorGray,
                    ),
                  ),
                  const SizedBox(
                    width: 4,
                  ),
                  Image.asset(
                    "assets/images/ic_map_pin_new.png",
                    width: 20,
                    height: 20,
                  ),
                  const Text(
                    "163,087",
                    style: TextStyle(
                      fontSize: 10,
                      color: AppColors.colorGray,
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 8, left: 8, right: 8),
          child: Row(
            children: [
              isAgent == true
                  ? Expanded(
                      flex: 3,
                      child: Row(
                        children: [
                          Image.asset(
                            "assets/images/ic_accept_agent.png",
                            width: 20,
                            height: 20,
                          ),
                          const SizedBox(
                            width: 4,
                          ),
                          const Flexible(
                            child: Text(
                              "Accept Agent",
                              style: TextStyle(
                                fontSize: 10,
                                color: AppColors.colorBlue,
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                          ),
                        ],
                      ),
                    )
                  : Container(),
              const Expanded(
                flex: 5,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Text(
                      "ดันประกาศล่าสุด : ",
                      style: TextStyle(
                        fontSize: 10,
                        color: AppColors.colorGray,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                    Flexible(
                      child: Text(
                        "เมื่อสักครู่",
                        style: TextStyle(
                          fontSize: 10,
                          color: AppColors.colorGray,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
