import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class CardBox extends StatelessWidget {
  final String title;
  final double amount;
  final Color color;
  final double size;
  final String unit;

  const CardBox(this.title, this.amount, this.color, this.size, this.unit, {super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(8),
      decoration: BoxDecoration(
        color: color,
        borderRadius: BorderRadius.circular(16),
      ),
      height: size,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            title,
            style: const TextStyle(fontSize: 16, color: Colors.white, fontWeight: FontWeight.bold),
          ),
          Expanded(
            child: Text(
              "${NumberFormat("#,###.##").format(amount)} $unit",
              style: const TextStyle(fontSize: 16, color: Colors.white, fontWeight: FontWeight.normal),
              textAlign: TextAlign.right,
            ),
          ),
        ],
      ),
    );
  }
}
