import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:intl/intl.dart';
import 'package:myfirstflutter/constant/colors.dart';
import 'package:myfirstflutter/di/cubit/ExchangeCubit.dart';
import 'package:myfirstflutter/di/cubit/ExchangeState.dart';
import 'package:myfirstflutter/widget/CustomElevatedButton.dart';
import 'widget/CardBox.dart';

class ExchangePage extends StatefulWidget {
  const ExchangePage({super.key});

  @override
  State<ExchangePage> createState() => _ExchangePageState();
}

class _ExchangePageState extends State<ExchangePage> {
  final ExchangeCubit _cubit = ExchangeCubit();
  late String _baseCurrency;

  @override
  void initState() {
    super.initState();
    _baseCurrency = "THB";
  }

  @override
  void dispose() {
    _cubit.resetState();
    _cubit.close();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: BlocBuilder<ExchangeCubit, ExchangeState>(
          bloc: _cubit,
          builder: (context, state) {
            if (state is ErrorState) {
              return Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Text('Error State'),
                    CustomElevatedButton(
                      buttonText: "Retry",
                      onPress: () {
                        _cubit.getExchangeRate(baseCurrency: _baseCurrency);
                      },
                    )
                  ],
                ),
              );
            } else if (state is LoadingState) {
              return const SpinKitCircle(
                color: Colors.green,
              );
            } else {
              final rates = _cubit.state.exchangeRate;
              return Padding(
                padding: const EdgeInsets.all(8.0),
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      CustomElevatedButton(
                        buttonText: "Choose Base Currency",
                        buttonColor: AppColors.colorPurple,
                        onPress: () {
                          _showBottomSheet();
                        },
                        startImageAsset: const Icon(
                          Icons.attach_money,
                          size: 24,
                          color: AppColors.colorWhite,
                        ),
                      ),
                      Text(
                        "Date : ${DateFormat.yMMMd().format(rates!.date!)}",
                        style: const TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      CustomElevatedButton(
                        buttonText: "Error State",
                        onPress: () {
                          _cubit.getExchangeRate(baseCurrency: "test");
                        },
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Expanded(
                            child: _cubit.state.amount == 1
                                ? CustomElevatedButton(
                                    buttonText: "1",
                                    onPress: () {
                                      setAmount(1);
                                    },
                                    buttonColor: AppColors.colorRedBright,
                                  )
                                : CustomElevatedButton(
                                    buttonText: "1",
                                    onPress: () {
                                      setAmount(1);
                                    },
                                    buttonColor: AppColors.colorLightGray,
                                  ),
                          ),
                          const SizedBox(
                            width: 4,
                          ),
                          Expanded(
                            child: _cubit.state.amount == 100
                                ? CustomElevatedButton(
                                    buttonText: "100",
                                    onPress: () {
                                      setAmount(100);
                                    },
                                    buttonColor: AppColors.colorRedBright,
                                  )
                                : CustomElevatedButton(
                                    buttonText: "100",
                                    onPress: () {
                                      setAmount(100);
                                    },
                                    buttonColor: AppColors.colorLightGray,
                                  ),
                          ),
                          const SizedBox(
                            width: 4,
                          ),
                          Expanded(
                            child: _cubit.state.amount == 500
                                ? CustomElevatedButton(
                                    buttonText: "500",
                                    onPress: () {
                                      setAmount(500);
                                    },
                                    buttonColor: AppColors.colorRedBright,
                                  )
                                : CustomElevatedButton(
                                    buttonText: "500",
                                    onPress: () {
                                      setAmount(500);
                                    },
                                    buttonColor: AppColors.colorLightGray,
                                  ),
                          ),
                          const SizedBox(
                            width: 4,
                          ),
                          Expanded(
                            child: _cubit.state.amount == 1000
                                ? CustomElevatedButton(
                                    buttonText: "1000",
                                    onPress: () {
                                      setAmount(1000);
                                    },
                                    buttonColor: AppColors.colorRedBright,
                                  )
                                : CustomElevatedButton(
                                    buttonText: "1000",
                                    onPress: () {
                                      setAmount(1000);
                                    },
                                    buttonColor: AppColors.colorLightGray,
                                  ),
                          ),
                        ],
                      ),
                      const SizedBox(height: 8.0),
                      CardBox("Currency : ${rates.base}", _cubit.state.amount, AppColors.colorBlue, 150, ""),
                      _baseCurrency != "THB"
                          ? Column(
                              children: [
                                const SizedBox(height: 8.0),
                                CardBox("${rates.base} -> THB", _cubit.state.amount * rates.rates!["THB"]!,
                                    AppColors.colorPrimary, 100, "baht"),
                              ],
                            )
                          : const SizedBox.shrink(),
                      _baseCurrency != "USD"
                          ? Column(
                              children: [
                                const SizedBox(height: 8.0),
                                CardBox("${rates.base} -> USD", _cubit.state.amount * rates.rates!["USD"]!,
                                    AppColors.colorPrimary, 100, "\$"),
                              ],
                            )
                          : const SizedBox.shrink(),
                      _baseCurrency != "EUR"
                          ? Column(
                              children: [
                                const SizedBox(height: 8.0),
                                CardBox("${rates.base} -> EUR", _cubit.state.amount * rates.rates!["EUR"]!,
                                    AppColors.colorPrimary, 100, "€"),
                              ],
                            )
                          : const SizedBox.shrink(),
                      _baseCurrency != "JPY"
                          ? Column(
                              children: [
                                const SizedBox(height: 8.0),
                                CardBox("${rates.base} -> JPY", _cubit.state.amount * rates.rates!["JPY"]!,
                                    AppColors.colorPrimary, 100, "¥"),
                              ],
                            )
                          : const SizedBox.shrink(),
                      _baseCurrency != "KRW"
                          ? Column(
                              children: [
                                const SizedBox(height: 8.0),
                                CardBox("${rates.base} -> KRW", _cubit.state.amount * rates.rates!["KRW"]!,
                                    AppColors.colorPrimary, 100, "won"),
                              ],
                            )
                          : const SizedBox.shrink(),
                    ],
                  ),
                ),
              );
            }
          },
        ),
      ),
    );
  }

  void setAmount(double amount) {
    _cubit.setAmount(amount);
  }

  _showBottomSheet() {
    showModalBottomSheet(
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(8),
          topRight: Radius.circular(8),
        ),
      ),
      context: context,
      builder: (context) {
        return Wrap(
          children: [
            ListTile(
              leading: _baseCurrency == "THB"
                  ? const Icon(
                      Icons.monetization_on_outlined,
                      color: AppColors.colorPrimary,
                    )
                  : const Icon(
                      Icons.monetization_on_outlined,
                      color: AppColors.colorDarkGray,
                    ),
              title: _baseCurrency == "THB"
                  ? const Text(
                      'THB',
                      style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.bold,
                        color: AppColors.colorPrimary,
                      ),
                    )
                  : const Text(
                      'THB',
                      style: TextStyle(
                        fontSize: 12,
                        color: AppColors.colorDarkGray,
                      ),
                    ),
              enabled: _baseCurrency != "THB",
              onTap: () {
                _baseCurrency = "THB";
                _cubit.getExchangeRate(baseCurrency: _baseCurrency);
                Navigator.pop(context);
              },
            ),
            const Divider(
              height: 20,
              thickness: 2,
            ),
            ListTile(
              leading: _baseCurrency == "USD"
                  ? const Icon(
                      Icons.monetization_on_outlined,
                      color: AppColors.colorPrimary,
                    )
                  : const Icon(
                      Icons.monetization_on_outlined,
                      color: AppColors.colorDarkGray,
                    ),
              title: _baseCurrency == "USD"
                  ? const Text(
                      'USD',
                      style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.bold,
                        color: AppColors.colorPrimary,
                      ),
                    )
                  : const Text(
                      'USD',
                      style: TextStyle(
                        fontSize: 12,
                        color: AppColors.colorDarkGray,
                      ),
                    ),
              enabled: _baseCurrency != "USD",
              onTap: () {
                _baseCurrency = "USD";
                _cubit.getExchangeRate(baseCurrency: _baseCurrency);
                Navigator.pop(context);
              },
            ),
            const Divider(
              height: 20,
              thickness: 2,
            ),
            ListTile(
              leading: _baseCurrency == "EUR"
                  ? const Icon(
                      Icons.monetization_on_outlined,
                      color: AppColors.colorPrimary,
                    )
                  : const Icon(
                      Icons.monetization_on_outlined,
                      color: AppColors.colorDarkGray,
                    ),
              title: _baseCurrency == "EUR"
                  ? const Text(
                      'EUR',
                      style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.bold,
                        color: AppColors.colorPrimary,
                      ),
                    )
                  : const Text(
                      'EUR',
                      style: TextStyle(
                        fontSize: 12,
                        color: AppColors.colorDarkGray,
                      ),
                    ),
              enabled: _baseCurrency != "EUR",
              onTap: () {
                _baseCurrency = "EUR";
                _cubit.getExchangeRate(baseCurrency: _baseCurrency);
                Navigator.pop(context);
              },
            ),
            const Divider(
              height: 20,
              thickness: 2,
            ),
            ListTile(
              leading: _baseCurrency == "JPY"
                  ? const Icon(
                      Icons.monetization_on_outlined,
                      color: AppColors.colorPrimary,
                    )
                  : const Icon(
                      Icons.monetization_on_outlined,
                      color: AppColors.colorDarkGray,
                    ),
              title: _baseCurrency == "JPY"
                  ? const Text(
                      'JPY',
                      style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.bold,
                        color: AppColors.colorPrimary,
                      ),
                    )
                  : const Text(
                      'JPY',
                      style: TextStyle(
                        fontSize: 12,
                        color: AppColors.colorDarkGray,
                      ),
                    ),
              enabled: _baseCurrency != "JPY",
              onTap: () {
                _baseCurrency = "JPY";
                _cubit.getExchangeRate(baseCurrency: _baseCurrency);
                Navigator.pop(context);
              },
            ),
            const Divider(
              height: 20,
              thickness: 2,
            ),
            ListTile(
              leading: _baseCurrency == "KRW"
                  ? const Icon(
                      Icons.monetization_on_outlined,
                      color: AppColors.colorPrimary,
                    )
                  : const Icon(
                      Icons.monetization_on_outlined,
                      color: AppColors.colorDarkGray,
                    ),
              title: _baseCurrency == "KRW"
                  ? const Text(
                      'KRW',
                      style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.bold,
                        color: AppColors.colorPrimary,
                      ),
                    )
                  : const Text(
                      'KRW',
                      style: TextStyle(
                        fontSize: 12,
                        color: AppColors.colorDarkGray,
                      ),
                    ),
              enabled: _baseCurrency != "KRW",
              onTap: () {
                _baseCurrency = "KRW";
                _cubit.getExchangeRate(baseCurrency: _baseCurrency);
                Navigator.pop(context);
              },
            ),
          ],
        );
      },
    );
  }
}
