import 'package:flutter/material.dart';
import 'package:myfirstflutter/constant/colors.dart';
import 'package:myfirstflutter/widget/CustomElevatedButton.dart';
import '../tab_layout/Page2/widget/BuildingTypeCard.dart';
import '../tab_layout/Page2/widget/PostTypeCard.dart';
import '../tab_layout/Page2/widget/PriceCard.dart';

class Tab1 extends StatefulWidget {
  const Tab1({super.key});

  @override
  State<Tab1> createState() => _Tab1State();
}

class _Tab1State extends State<Tab1> {
  int _direction = 0;

  List<Widget> listUtility = [
    Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Image.asset(
          "assets/images/ic_house_plan_outlined.png",
          width: 20,
          height: 20,
        ),
        const Flexible(
          child: Text(
            "120.00 ตร.ม",
            style: TextStyle(
              fontSize: 10,
              overflow: TextOverflow.ellipsis,
            ),
          ),
        ),
      ],
    ),
    Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Image.asset(
          "assets/images/ic_house_plan_outlined.png",
          width: 20,
          height: 20,
        ),
        const Flexible(
          child: Text(
            "120.00 ตร.ม",
            style: TextStyle(
              fontSize: 10,
              overflow: TextOverflow.ellipsis,
            ),
          ),
        ),
      ],
    ),
    Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Image.asset(
          "assets/images/ic_house_plan_outlined.png",
          width: 20,
          height: 20,
        ),
        const Flexible(
          child: Text(
            "120.00 ตร.ม",
            style: TextStyle(
              fontSize: 10,
              overflow: TextOverflow.ellipsis,
            ),
          ),
        ),
      ],
    ),
    Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Image.asset(
          "assets/images/ic_house_plan_outlined.png",
          width: 20,
          height: 20,
        ),
        const Flexible(
          child: Text(
            "120.00 ตร.ม",
            style: TextStyle(
              fontSize: 10,
              overflow: TextOverflow.ellipsis,
            ),
          ),
        ),
      ],
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              padding: const EdgeInsets.all(8),
              child: CustomElevatedButton(
                  buttonText: "Change ItemGrid",
                  onPress: () {
                    setDirection();
                  }),
            ),
            GridView.count(
                physics: const NeverScrollableScrollPhysics(),
                scrollDirection: Axis.vertical,
                crossAxisCount: _direction == 0 ? 2 : 1,
                mainAxisSpacing: 16,
                crossAxisSpacing: 16,
                childAspectRatio: _direction == 0
                    ? listUtility.isNotEmpty
                        ? MediaQuery.of(context).size.height / 1850
                        : MediaQuery.of(context).size.height / 1550
                    : listUtility.isNotEmpty
                        ? MediaQuery.of(context).size.height / 750
                        : MediaQuery.of(context).size.height / 700,
                shrinkWrap: true,
                padding: const EdgeInsets.all(8),
                children: [
                  stockItem(),
                  stockItem(),
                  stockItem(),
                  stockItem(),
                  stockItem(),
                  stockItem(),
                ]),
          ],
        ),
      ),
    );
  }

  void setDirection() {
    setState(() {
      if (_direction == 0) {
        _direction = 1;
      } else {
        _direction = 0;
      }
    });
  }

  Widget stockItem() {
    return Container(
      decoration: BoxDecoration(
        color: AppColors.colorWhite,
        borderRadius: BorderRadius.circular(16),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 5,
            blurRadius: 8,
          ),
        ],
      ),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        children: [
          AspectRatio(
            aspectRatio: _direction == 0 ? 10 / 9 : 21 / 9,
            child: Stack(
              fit: StackFit.expand,
              children: [
                ClipRRect(
                  borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(8.0),
                    topRight: Radius.circular(8.0),
                  ),
                  child: Image.asset("assets/images/living_room.png", fit: BoxFit.cover),
                ),
                const Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Row(
                        children: [
                          BuildingTypeCard("บ้าน", Colors.green),
                          SizedBox(
                            width: 4,
                          ),
                          PostTypeCard("ขาย", Colors.green)
                        ],
                      ),
                    ),
                    PriceCard("7,000,000", "10,000,000", "อายุประกาศที่เหลือ 9999 วัน", false)
                  ],
                ),
              ],
            ),
          ),
          _contentView(),
        ],
      ),
    );
  }

  Widget _contentView() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Padding(
          padding: EdgeInsets.all(8.0),
          child: Row(
            children: [
              Expanded(
                child: Text(
                  "ให้เช่าคอนโดหรูทำเลพระราม 9 แค่ 7,000 บาท เท่านั้นหาราคาแบบนี้ ในพระราม 9",
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold),
                ),
              )
            ],
          ),
        ),
        const Padding(
          padding: EdgeInsets.all(8),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                "แก้ไขล่าสุด 30/01/2565",
                style: TextStyle(fontSize: 8, color: AppColors.colorGray),
              ),
              Flexible(
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Icon(
                      Icons.ads_click,
                      size: 18,
                    ),
                    SizedBox(
                      width: 4,
                    ),
                    Flexible(
                      child: Text(
                        "12000",
                        style: TextStyle(
                          fontSize: 8,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        Container(
          padding: const EdgeInsets.all(8),
          color: AppColors.colorBlue.withOpacity(0.1),
          child: Row(
            children: [
              Image.asset(
                "assets/images/ic_map_pin_new.png",
                width: 20,
                height: 20,
              ),
              const SizedBox(
                width: 4,
              ),
              const Flexible(
                child: Text(
                  "ไลฟ์ อโศก-พระราม9",
                  style: TextStyle(
                    fontSize: 10,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8),
          child: _direction == 0
              ? Column(
                  children: listUtility.length <= 4
                      ? listUtility
                      : [
                          listUtility[0],
                          listUtility[1],
                          listUtility[2],
                          listUtility[3],
                        ],
                )
              : SizedBox(
                  height: 30,
                  child: ListView(
                    scrollDirection: Axis.horizontal,
                    children: listUtility.length <= 4
                        ? listUtility
                        : [
                            listUtility[0],
                            listUtility[1],
                            listUtility[2],
                            listUtility[3],
                          ],
                  ),
                ),
        ),
      ],
    );
  }
}
