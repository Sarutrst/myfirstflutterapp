import 'package:flutter/material.dart';
import 'package:myfirstflutter/constant/routes.dart';
import '../../constant/colors.dart';
import '../../di/route/app_route.dart';

class Tab2 extends StatelessWidget {
  const Tab2({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SizedBox(
        width: MediaQuery.of(context).size.width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            ElevatedButton(
              onPressed: () {
                Navigator.pushNamed(context, AppRoutes.secondPage, arguments: SecondPageArguments(initPage: 0));
              },
              child: const Text(
                'ChangePage Tab1',
                style: TextStyle(color: AppColors.colorWhite),
              ),
            ),
            ElevatedButton(
              onPressed: () {
                Navigator.pushNamed(context, AppRoutes.secondPage, arguments: SecondPageArguments(initPage: 1));
              },
              child: const Text(
                'ChangePage Tab2',
                style: TextStyle(color: AppColors.colorWhite),
              ),
            ),
            ElevatedButton(
              onPressed: () {
                Navigator.pushNamed(context, AppRoutes.thirdPage);
              },
              child: const Text(
                'Third Page',
                style: TextStyle(color: AppColors.colorWhite),
              ),
            )
          ],
        ),
      ),
    );
  }
}
