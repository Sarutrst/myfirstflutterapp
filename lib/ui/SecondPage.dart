import 'package:flutter/material.dart';
import 'package:myfirstflutter/constant/colors.dart';
import 'package:myfirstflutter/ui/tab_layout/Page1.dart';
import 'package:myfirstflutter/ui/tab_layout/Page2/Page2.dart';

class SecondPage extends StatefulWidget {
  final int? initPage;

  @override
  SecondPageState createState() => SecondPageState();

  const SecondPage({Key? key, this.initPage}) : super(key: key);
}

class SecondPageState extends State<SecondPage> with SingleTickerProviderStateMixin, RestorationMixin {
  TabController? _tabController;
  late RestorableInt tabIndex;

  final List<Widget> tabs = const [
    Page1(),
    Page2(),
  ];

  @override
  String get restorationId => 'tab_scrollable_demo';

  @override
  void restoreState(RestorationBucket? oldBucket, bool initialRestore) {
    registerForRestoration(tabIndex, 'tab_index');
    _tabController?.index = tabIndex.value;
  }

  @override
  void initState() {
    tabIndex = RestorableInt(widget.initPage ?? 0);
    _tabController = TabController(
      initialIndex: widget.initPage ?? 0,
      length: tabs.length,
      vsync: this,
    );
    _tabController!.addListener(() {
      // When the tab controller's value is updated, make sure to update the
      // tab index value, which is state restorable.
      setState(() {
        tabIndex.value = _tabController!.index;
      });
    });
    super.initState();
  }

  @override
  void dispose() {
    _tabController!.dispose();
    tabIndex.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: const Text(
          "SecondPage",
          style: TextStyle(color: AppColors.colorWhite),
        ),
        bottom: TabBar(
          controller: _tabController,
          isScrollable: false,
          indicatorColor: AppColors.colorRedBright,
          unselectedLabelColor: AppColors.colorGray,
          labelColor: AppColors.colorRedBright,
          tabs: const <Tab>[
            Tab(
              child: Column(
                children: <Widget>[
                  Icon(Icons.accessibility_new_sharp, size: 20),
                  Text(
                    "Page1",
                    style: TextStyle(fontSize: 12),
                  ),
                ],
              ),
            ),
            Tab(
              child: Column(
                children: <Widget>[
                  Icon(Icons.pest_control_outlined, size: 20),
                  Text(
                    "Page2",
                    style: TextStyle(fontSize: 12),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      body: TabBarView(
        controller: _tabController,
        children: tabs,
      ),
    );
  }
}
