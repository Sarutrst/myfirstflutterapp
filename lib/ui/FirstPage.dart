import 'package:flutter/material.dart';
import 'package:myfirstflutter/constant/colors.dart';
import 'package:myfirstflutter/ui/bottom_navigation/ExchangeRate/ExchangePage.dart';
import 'package:myfirstflutter/ui/bottom_navigation/Tab2.dart';
import 'bottom_navigation/Tab1.dart';

class FirstPage extends StatefulWidget {
  const FirstPage({super.key});

  @override
  State<FirstPage> createState() => _FirstPageState();
}

class _FirstPageState extends State<FirstPage> {
  int _selectedIndex = 0;

  final List<Widget> tabs = const [
    ExchangePage(),
    Tab1(),
    Tab2(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "FirstPage",
          style: TextStyle(color: AppColors.colorWhite),
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.attach_money),
            label: 'Exchange',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.tab),
            label: 'Tab1',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.tab),
            label: 'Tab2',
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: AppColors.colorRedBright,
        onTap: (int index) {
          setState(() {
            _selectedIndex = index;
          });
        },
      ),
      body: Center(
        child: tabs.elementAt(_selectedIndex),
      ),
    );
  }
}
