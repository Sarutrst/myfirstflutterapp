import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get_navigation/src/root/get_material_app.dart';
import 'package:loader_overlay/loader_overlay.dart';
import 'package:myfirstflutter/constant/colors.dart';
import 'package:myfirstflutter/constant/fonts.dart';
import 'package:myfirstflutter/constant/routes.dart';
import 'package:myfirstflutter/main_app_route.dart';
import 'di/cubit/ExchangeCubit.dart';

void main() async {
  await ScreenUtil.ensureScreenSize();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context);
    return GestureDetector(
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
      },
      child: GetMaterialApp(
        builder: (context, child) {
          return MultiBlocProvider(
            providers: [
              BlocProvider<Cubit>(
                create: (context) => ExchangeCubit(),
              )
            ],
            child: MediaQuery(
              data: MediaQuery.of(context).copyWith(
                textScaleFactor: 1.0,
              ),
              child: LoaderOverlay(
                useDefaultLoading: false,
                overlayWidget: IgnorePointer(
                    ignoring: true,
                    child: Container(
                        color: Colors.transparent.withOpacity(0.6),
                        child: Center(
                          child: SpinKitFadingCircle(
                              size: 30.w,
                              itemBuilder: (BuildContext context, int index) {
                                return DecoratedBox(
                                  decoration: BoxDecoration(
                                      color: index.isEven ? Colors.grey : const Color(0x99E8EAF6).withOpacity(0.2),
                                      shape: BoxShape.circle),
                                );
                              }),
                        ))),
                child: child!,
              ),
            ),
          );
        },
        theme: ThemeData(
            fontFamily: AppFonts.fontSarabunLight, primarySwatch: Colors.cyan, dividerColor: AppColors.colorDivider),
        initialRoute: AppRoutes.firstPage,
        debugShowCheckedModeBanner: false,
        routes: mainAppRoutes,
      ),
    );
  }
}
