import 'package:flutter/material.dart';
import 'package:myfirstflutter/constant/routes.dart';
import 'package:myfirstflutter/ui/FirstPage.dart';
import 'package:myfirstflutter/ui/SecondPage.dart';
import 'package:myfirstflutter/ui/ThirdPage.dart';

final Map<String, Widget Function(BuildContext)> appRoute = {
  AppRoutes.firstPage: (context) {
    return FirstPage();
  },
  AppRoutes.secondPage: (context) {
    final args = ModalRoute.of(context)!.settings.arguments as SecondPageArguments;
    return SecondPage(
      initPage: args.initPage,
    );
  },
  AppRoutes.thirdPage: (context) {
    return ThirdPage();
  }
};

class SecondPageArguments {
  SecondPageArguments({this.initPage});

  final int? initPage;
}
