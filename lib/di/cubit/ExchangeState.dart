import 'package:equatable/equatable.dart';

import '../../data/ExchangeRate.dart';

class ExchangeState extends Equatable {
  const ExchangeState({
    this.amount = 1,
    this.exchangeRate,
  });

  final double amount;
  final ExchangeRate? exchangeRate;

  ExchangeState copyWith({
    double? amount,
    ExchangeRate? exchangeRate,
  }) {
    return ExchangeState(
      amount: amount ?? this.amount,
      exchangeRate: exchangeRate ?? this.exchangeRate,
    );
  }

  @override
  List<Object?> get props => [
        amount,
        exchangeRate,
      ];
}

class InitState extends ExchangeState {
  @override
  List<Object> get props => [];
}

class LoadingState extends ExchangeState {
  @override
  List<Object> get props => [];
}

class ErrorState extends ExchangeState {
  @override
  List<Object> get props => [];
}
