import 'package:bloc/bloc.dart';
import 'package:myfirstflutter/di/cubit/ExchangeState.dart';
import '../../api/ExchangeRateAPI.dart';

class ExchangeCubit extends Cubit<ExchangeState> {
  ExchangeCubit({ExchangeState? state}) : super(state ?? InitState()) {
    getExchangeRate();
  }

  void resetState() {
    emit(const ExchangeState());
  }

  void setAmount(double newAmount) {
    emit(state.copyWith(amount: newAmount));
  }

  void getExchangeRate({String baseCurrency = "THB"}) async {
    try {
      emit(LoadingState());
      final result = await ExchangeRateAPI().getExchangeRate(baseCurrency);
      emit(state.copyWith(exchangeRate: result));
    } catch (e) {
      emit(ErrorState());
    }
  }
}
