import 'package:flutter/material.dart';
import 'package:myfirstflutter/constant/colors.dart';

class CustomElevatedButton extends StatelessWidget {
  final Function() onPress;
  final String? buttonText;
  final Color? buttonColor;
  final Color? buttonBorderColor;
  final double? buttonColorOpacity;
  final double? buttonBorderColorOpacity;
  final double? buttonRadius;
  final double? buttonElevation;
  final bool? isSingleLine;
  final Icon? startImageAsset;
  final Gradient? gradient;
  final TextStyle? buttonTextStyle;

  const CustomElevatedButton({
    Key? key,
    this.buttonColor,
    this.buttonBorderColor,
    this.buttonColorOpacity,
    this.buttonBorderColorOpacity,
    this.buttonRadius,
    this.buttonElevation,
    this.isSingleLine,
    this.startImageAsset,
    this.gradient,
    this.buttonTextStyle,
    required this.buttonText,
    required this.onPress,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        gradient: gradient,
        borderRadius: BorderRadius.circular(buttonRadius ?? 8),
      ),
      child: ElevatedButton.icon(
        style: ElevatedButton.styleFrom(
          elevation: buttonElevation ?? 0,
          backgroundColor: gradient != null
              ? Colors.transparent
              : buttonColor?.withOpacity(buttonColorOpacity ?? 1) ??
                  AppColors.colorPrimary.withOpacity(buttonColorOpacity ?? 1),
          padding: const EdgeInsets.all(8),
          shape: RoundedRectangleBorder(
            side: BorderSide(
              color: gradient != null
                  ? Colors.transparent
                  : buttonBorderColor?.withOpacity(buttonBorderColorOpacity ?? 1) ?? Colors.transparent,
            ),
            borderRadius: BorderRadius.circular(buttonRadius ?? 8),
          ),
        ),
        onPressed: () {
          onPress();
        },
        icon: startImageAsset ?? const SizedBox.shrink(),
        label: Text(
          buttonText ?? "",
          style: buttonTextStyle ?? const TextStyle(fontSize: 14, color: AppColors.colorWhite),
          overflow: isSingleLine ?? false ? TextOverflow.ellipsis : TextOverflow.visible,
        ),
      ),
    );
  }
}
