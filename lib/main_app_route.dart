import 'package:flutter/material.dart';
import 'di/route/app_route.dart';

final Map<String, Widget Function(BuildContext)> mainAppRoutes = {
  ...appRoute,
};
